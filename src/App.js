import React, { useState } from "react";
import Spinner from "./Spinner/Spinner";
import ChromeSpinner from "./ChromeSpinner/ChromeSpinner";
import "./App.css";

function App() {
	const [color, setColor] = useState("rgb(0, 0, 0)");
	const randomRGB1 = Math.floor(Math.random() * 255 + 1);
	const randomRGB2 = Math.floor(Math.random() * 255 + 1);
	const randomRGB3 = Math.floor(Math.random() * 255 + 1);
	const randomColor = `rgb(${randomRGB1}, ${randomRGB2}, ${randomRGB3})`;

	return (
		<div className="App">
			<Spinner size={3} color={color} />
			<ChromeSpinner size={3} color={color} />
			<button onClick={() => setColor(randomColor)}>Change Color</button>
		</div>
	);
}

export default App;
