import React from "react";
import "./chromeSpinner.scss";

const ChromeSpinner = ({ size, color }) => {
	const inlineStyles = {
		width: `${size * 50}px`,
		height: `${size * 50}px`
	};
	return (
		<div style={inlineStyles} className="svg-container">
			<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
				<circle stroke={`${color}`} cx="50" cy="50" r="45" />
			</svg>
		</div>
	);
};

export default ChromeSpinner;
