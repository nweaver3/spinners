import React from "react";
import "./spinner.scss";

const Spinner = ({ size, color }) => {
	const sizeCheck = size >= 4 ? 4 : size;
	const gridSizes = `calc(${sizeCheck * 50}px / 5)`;
	const inlineStyles = {
		width: `${sizeCheck * 50}px`,
		height: `${sizeCheck * 50}px`,
		gridTemplateColumns: `${gridSizes} ${gridSizes} ${gridSizes} ${gridSizes} ${gridSizes}`,
		gridTemplateRows: `${gridSizes} ${gridSizes} ${gridSizes} ${gridSizes} ${gridSizes}`
	};
	const dotStyles = {
		backgroundColor: color
	};
	return (
		<div style={inlineStyles} className="spinner-container">
			<div />
			<div>
				<span style={dotStyles} className="dot" />
			</div>
			<div>
				<span style={dotStyles} className="dot" />
			</div>
			<div>
				<span style={dotStyles} className="dot" />
			</div>
			<div />
			<div>
				<span style={dotStyles} className="dot" />
			</div>
			<div />
			<div />
			<div />
			<div>
				<span style={dotStyles} className="dot" />
			</div>
			<div>
				<span style={dotStyles} className="dot" />
			</div>
			<div />
			<div />
			<div />
			<div>
				<span style={dotStyles} className="dot" />
			</div>
			<div>
				<span style={dotStyles} className="dot" />
			</div>
			<div />
			<div />
			<div />
			<div>
				<span style={dotStyles} className="dot" />
			</div>
			<div />
			<div>
				<span style={dotStyles} className="dot" />
			</div>
			<div>
				<span style={dotStyles} className="dot" />
			</div>
			<div>
				<span style={dotStyles} className="dot" />
			</div>
			<div />
		</div>
	);
};

export default Spinner;
